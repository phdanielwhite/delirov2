<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imagenes extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'imagenes';
    protected $primaryKey = 'id_imagenes';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'url'
    ];

    public function ingrediente(){
        return $this->hasMany('App\IngredientesImagenes', 'id_imagenes', 'id_imagenes');
    }
    public function producto(){
        return $this->hasMany('App\ProductosImagenes', 'id_imagenes', 'id_imagenes');
    }
}
