<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosPedidos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_pedidos';
    protected $primaryKey = 'id_productos_pedidos';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_productos',
        'id_pedidos'
    ];

    public function producto(){
    	return $this->belongsTo('App\Productos', 'id_productos', 'id_productos');
    }
    public function pedido(){
        return $this->belongsTo('App\Pedidos', 'id_pedidos', 'id_pedidos');
    }
    public function ingrediente(){
        return $this->hasMany('App\IngredientesPedidos', 'id_productos_pedidos', 'id_productos_pedidos');
    }
}