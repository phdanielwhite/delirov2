<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredientes extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'ingredientes';
    protected $primaryKey = 'id_ingredientes';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'nombre',
        'descripcion',
        'codigo'
    ];

    public function producto(){
        return $this->hasMany('App\ProductosIngredientes', 'id_ingredientes', 'id_ingredientes');
    }
    public function imagenes(){
        return $this->hasMany('App\IngredientesImagenes', 'id_ingredientes', 'id_ingredientes');
    }
    public function pedido(){
        return $this->hasMany('App\IngredientesPedidos', 'id_ingredientes', 'id_ingredientes');
    }
}
