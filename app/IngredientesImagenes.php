<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientesImagenes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ingredientes_imagenes';
    protected $primaryKey = 'id_ingredientes_imagenes';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_ingredientes',
        'id_imagenes'
    ];

    public function ingredientes(){
    	return $this->belongsTo('App\Ingredientes', 'id_ingredientes', 'id_ingredientes');
    }
    public function imagenes(){
        return $this->belongsTo('App\Imagenes', 'id_imagenes', 'id_imagenes');
    }
}