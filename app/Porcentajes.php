<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Porcentajes extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'porcentajes';
    protected $primaryKey = 'id_porcentajes';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'nombre',
        'porcentaje',
    ];

    public function productos(){
        return $this->hasMany('App\ProductosPorcentajes', 'id_porcentajes', 'id_porcentajes');
    }
}
