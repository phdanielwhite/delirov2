<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientesPedidos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ingredientes_pedidos';
    protected $primaryKey = 'id_ingredientes_pedidos';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_productos_pedidos',
        'id_ingredientes'
    ];

    public function producto_pedido(){
    	return $this->belongsTo('App\ProductosPedidos', 'id_productos_pedidos', 'id_productos_pedidos');
    }
    public function ingrediente(){
        return $this->belongsTo('App\Ingredientes', 'id_ingredientes', 'id_ingredientes');
    }
}