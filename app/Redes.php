<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'redes';
    protected $primaryKey = 'id_redes';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_tipo_redes',
        'descripcion'
    ];

    public function tipo(){
    	return $this->belongsTo('App\TiposRedes', 'id_tipo_redes', 'id_tipo_redes');
    }
}