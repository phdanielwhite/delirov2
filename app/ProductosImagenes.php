<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosImagenes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_imagenes';
    protected $primaryKey = 'id_productos_categorias';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_productos',
        'id_imagenes'
    ];

    public function producto(){
    	return $this->belongsTo('App\Productos', 'id_productos', 'id_productos');
    }
    public function imagen(){
        return $this->belongsTo('App\Imagenes', 'id_imagenes', 'id_imagenes');
    }
}