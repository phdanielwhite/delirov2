<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productos extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'productos';
    protected $primaryKey = 'id_productos';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'nombre',
        'descripcion',
        'codigo',
        'precio'
    ];

    public function porcentaje(){
        return $this->hasMany('App\ProductosPorcentajes', 'id_productos', 'id_productos');
    }
    public function categoria(){
        return $this->hasMany('App\ProductosCategorias', 'id_productos', 'id_productos');
    }
    public function ingrediente(){
        return $this->hasMany('App\ProductosIngredientes', 'id_productos', 'id_productos');
    }
    public function pedido(){
        return $this->hasMany('App\ProductosPedidos', 'id_productos', 'id_productos');
    }
    public function imagenes(){
        return $this->hasMany('App\ProductosImagenes', 'id_productos', 'id_productos');
    }
    public function sucursal(){
        return $this->hasMany('App\ProductosSucursales', 'id_productos', 'id_productos');
    }
}
