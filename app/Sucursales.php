<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursales extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'sucursales';
    protected $primaryKey = 'id_sucursales';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'nombre',
        'descripcion',
        'abierto',
        'numero_whatsapp'
    ];

    public function red(){
        return $this->hasMany('App\Redes', 'id_sucursales', 'id_sucursales');
    }
    public function producto(){
        return $this->hasMany('App\Productos', 'id_sucursales', 'id_sucursales');
    }
}
