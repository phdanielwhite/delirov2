<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosCategorias extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_categorias';
    protected $primaryKey = 'id_productos_categorias';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_productos',
        'id_categorias'
    ];

    public function producto(){
    	return $this->belongsTo('App\Productos', 'id_productos', 'id_productos');
    }
    public function categoria(){
        return $this->belongsTo('App\Categorias', 'id_categorias', 'id_categorias');
    }
}