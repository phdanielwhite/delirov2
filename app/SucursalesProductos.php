<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalesProductos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'sucursales_productos';
    protected $primaryKey = 'id_sucursales_productos';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_sucursales',
        'id_productos',
        'disponible'
    ];

    public function sucursal(){
    	return $this->belongsTo('App\Sucursales', 'id_sucursales', 'id_sucursales');
    }
    public function producto(){
        return $this->belongsTo('App\Productos', 'id_productos', 'id_productos');
    }
}