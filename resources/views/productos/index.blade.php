@extends ('admin.layout')
@section ('content')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<legend>Listado de productos</legend>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" id="example">
				<thead>
					<th>Nombre</th>
					<th>Precio</th>
					<th>Descripcion</th>
				</thead>
				<tbody>
				@foreach($productos as $p)
				<tr>
					<td>{{$p->nombre}}</td>
					<td>{{$p->precio}}</td>
					<td>{{$p->descripcion}}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col col-md-3 col-md-offset-4" align="center">
		<a href="{{ URL::action('ProductosController@create') }}" class="btn btn-info btn-md" title="Agregar nuevo producto">Nuevo Producto</a>
	</div>
</div>
@endsection