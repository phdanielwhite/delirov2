<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosIngredientes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_ingredientes';
    protected $primaryKey = 'id_productos_ingredientes';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_productos',
        'id_ingredientes'
    ];

    public function producto(){
    	return $this->belongsTo('App\Productos', 'id_productos', 'id_productos');
    }
    public function ingrediente(){
        return $this->belongsTo('App\Ingredientes', 'id_ingredientes', 'id_ingredientes');
    }
}