<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposRedes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipo_redes';
    protected $primaryKey = 'id_tipo_redes';

    public $timestamps = false;
    
    protected $fillable = [
    	'nombre',
        'id_categorias'
    ];

    public function red(){
        return $this->belongsTo('App\Redes', 'id_tipo_redes', 'id_tipo_redes');
    }
}