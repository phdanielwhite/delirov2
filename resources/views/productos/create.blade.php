@extends ('admin.layout')
@section ('content')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<legend>Nuevo producto</legend>
		<hr>
		<form id="formvalidate" action="{{ URL::action('ProductosController@store') }}" enctype="multipart/form-data" method="post">
			@csrf
			<div class="row">
				<div class="col col-md-3">
					<div class="form-group">
						<label for="nombre">Nombre (*)</label>
						<input type="text" name="nombre" id="nombre" class="form-control" required>
					</div>
				</div>
				<div class="col col-md-5">
					<div class="form-group">
						<label for="descripcion">Descripción</label>
						<input type="text" name="descripcion" id="descripcion" class="form-control">
					</div>
				</div>
				<div class="col col-md-2">
					<div class="form-group">
						<label for="codigo">Código</label>
						<input type="text" name="codigo" id="codigo" class="form-control">
					</div>			
				</div>
				<div class="col col-md-2">
					<div class="form-group">
						<label for="precio">Precio</label>
						<input type="text" name="precio" id="precio" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col col-md-5">
					<div class="form-group">
						<label for="categoria">Categorias</label>
						<select class="form-control" multiple="multiple" id="categoria" name="categoria[]" style="width: 100%;">
							<option></option>
			                @foreach ($categorias as $c)
			                    <option value="{{ $c->id_categorias }}">
			                        {!! $c->nombre !!}
			                    </option>
			                @endforeach
			            </select>
					</div>
				</div>
				<div class="col col-md-5">
					<div class="form-group">
						<label for="ingrediente">Ingredientes</label>
						<select class="form-control" multiple="multiple" id="ingrediente" name="ingrediente[]" style="width: 100%;">
							<option></option>
			                @foreach ($ingredientes as $i)
			                    <option value="{{ $i->id_ingredientes }}">
			                        {!! $i->nombre !!}
			                    </option>
			                @endforeach
			            </select>
					</div>
				</div>
				<div class="col col-md-2">
					<div class="form-group">
						<label for="descuento">Descuento</label>
						<select class="form-control" id="descuento" name="descuento" style="width: 100%;">
							<option></option>
			                @foreach ($descuentos as $d)
			                    <option value="{{ $d->id_descuentos }}">
			                        {!! $d->nombre !!}
			                    </option>
			                @endforeach
			            </select>
					</div>
				</div>		
			</div>
			</br>
	        <div class="row justify-content-md-center">
				<div class="col col-md-3">
					<button class="btn btn-info btn-block btn-md" id="agregar" type="submit" title="Agregar nuevo contribuyente">Agregar</button>
				</div>
				<div class="col col-md-3">
					<button class="btn btn-danger btn-block btn-md" onclick="history.back(-1)" type="reset" title="Cancelar">Cancelar</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('scripts')
	<script>
		$('#categoria').select2({
	    	placeholder: "Categorias"
	    });
	    $('#ingrediente').select2({
	    	placeholder: "Ingredientes"
	    });
	    $('#descuento').select2({
	    	placeholder: "%"
	    });
	</script>


@endpush