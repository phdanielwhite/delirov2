<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Productos;
use App\Categorias;
use App\Ingredientes;
use App\Porcentajes;
use App\ProductosCategorias;
use App\ProductosIngredientes;

class ProductosController extends Controller
{
    public function index(Request $request){
        $productos = Productos::get();
        return view("productos.index", ["productos"=>$productos]);
    }
    public function create(){
        $categorias = Categorias::get();
        $ingredientes = Ingredientes::get();
        $descuentos = Porcentajes::get();

        return view("productos.create",[
            "categorias" => $categorias,
            "ingredientes" => $ingredientes,
            "descuentos" => $descuentos
        ]);
    }
    public function store(Request $request){
        $producto = new Productos();
        $producto->fill([
            'nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'codigo'  => $request->input('codigo'),
            'precio' => $request->input('precio')
        ]);
        $producto->save();

        if($request->has('categoria')){
            foreach ($request->input('categoria') as $cat) {
                $categorias = new ProductosCategorias();
                $categorias->fill([
                    'id_productos' => $producto->id_productos,
                    'id_categorias' => $cat
                 ]);
                $categorias->save();
            }
        }

        if($request->has('ingrediente')){
            foreach ($request->input('ingrediente') as $ing) {
                $ingredientes = new ProductosIngredientes();
                $ingredientes->fill([
                    'id_productos' => $producto->id_productos,
                    'id_ingredientes' => $ing
                 ]);
                $ingredientes->save();
            }
        }


        return redirect('/productos');
    }
    public function show($id){
    }
    public function edit($id){
    }
    public function update(Request $request, $id){
    }
    public function destroy(Request $request, $id){
    }

}
