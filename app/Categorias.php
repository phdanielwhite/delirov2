<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categorias extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'categorias';
    protected $primaryKey = 'id_categorias';

    public $timestamps = true;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'nombre',
        'descripcion'
    ];

    public function productos(){
        return $this->hasMany('App\ProductosCategorias', 'id_categorias', 'id_categorias');
    }
}
