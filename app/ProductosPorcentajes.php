<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosPorcentajes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_porcentajes';
    protected $primaryKey = 'id_productos_porcentajes';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_porcentajes',
        'id_categorias'
    ];

    public function porcentaje(){
    	return $this->belongsTo('App\Porcentajes', 'id_porcentajes', 'id_porcentajes');
    }
    public function categorias(){
        return $this->belongsTo('App\Categorias', 'id_categorias', 'id_categorias');
    }
}