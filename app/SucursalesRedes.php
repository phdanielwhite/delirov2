<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalesRedes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'sucursales_redes';
    protected $primaryKey = 'id_sucursales_redes';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_redes',
        'id_sucursales',
        'estado'
    ];

    public function red(){
    	return $this->belongsTo('App\Redes', 'id_redes', 'id_redes');
    }
    public function sucursal(){
        return $this->belongsTo('App\Sucursales', 'id_sucursales', 'id_sucursales');
    }
}