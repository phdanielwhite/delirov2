<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pedidos';
    protected $primaryKey = 'id_pedidos';

    public $timestamps = false;
    
    protected $fillable = [
    	'id_sucursales',
        'fecha',
        'hora',
        'telefono',
        'cliente',
        'total',
        'detalles',
        'estado'
    ];

    public function sucursal(){
    	return $this->belongsTo('App\Sucursales', 'id_sucursales', 'id_sucursales');
    }
    public function producto(){
        return $this->hasMany('App\ProductosPedidos', 'id_pedidos', 'id_pedidos');
    }
}